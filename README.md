## Bastion Blocks

# Getting Started #

1. Install Nuget packages into your application.

    ```
    Package Manager : Install-Package Bastion.Blocks
    CLI : dotnet add package Bastion.Blocks
	```
	```
	Package Manager : Install-Package Bastion.Blocks.EntityFrameworkCore
    CLI : dotnet add package Bastion.Blocks.EntityFrameworkCore
    ```

2. In the `ConfigureServices` method of `Startup.cs`.
    
    ```csharp
    services.AddBlocksService();
    ```

	```csharp
    services.AddBlocksRepository(
        blockOptions => blockOptions.Project = "default",
        options => options.UseNpgsql(Configuration.GetConnectionString("pgSqlConnection"),
        builder => builder.MigrationsAssembly("BastionBlocksSample.Api")));
    ```
3. Create a migration and update the database

	```
	Package Manager : Add-Migration InitialCreate
	Package Manager : Update-Database
	```

4. If you need web api, install the Nuget package.

	```
	Package Manager : Install-Package Bastion.Blocks.Api
    CLI : dotnet add package Bastion.Blocks.Api
    ```