using Bastion.Authentication.Basic;
using Bastion.Blocks.Api.Models.Requests;
using Bastion.Blocks.Api.Responses;

using BastionBlocksSample.Api;

using Blocks.Api.FunctionalTests.Extensions;

using Microsoft.Extensions.DependencyInjection;

using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

using Xunit;

namespace Blocks.Api.FunctionalTests
{
    [ExcludeFromCodeCoverage]
    public class FullAccessScenario : IClassFixture<ScenarioFactory<Startup>>
    {
        private readonly ScenarioFactory<Startup> factory;

        public FullAccessScenario(ScenarioFactory<Startup> factory)
        {
            this.factory = factory;
        }

        private readonly AuthenticationHeaderValue authorizationAdmin = new AuthenticationHeaderValue(
            BasicAuthenticationDefaults.AuthenticationScheme, Convert.ToBase64String(Encoding.ASCII.GetBytes("admin:password")));

        [Fact(DisplayName = "Add Get Update Get Delete Get Block")]
        public async Task Add_Get_Update_Get_Delete_Get_Block()
        {
            var blockCreate = new BlockCreateRequest
            {
                Text = "Text-1",
                Title = "Title-1",
                Section = "Section-1",
            };

            using var client = factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = authorizationAdmin;
            var response = await client.SendAsync(new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks"),
                Content = new StringContent(JsonSerializer.Serialize(blockCreate), Encoding.UTF8, MediaTypeNames.Application.Json),
            });
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

            var blockCreated = await response.ReadAsAsync<BlockResponse>();
            Assert.NotNull(blockCreated);

            Assert.NotNull(blockCreated.BlockId);
            Assert.Equal(blockCreated.Text, blockCreate.Text);
            Assert.Equal(blockCreated.Title, blockCreate.Title);
            Assert.Equal(blockCreated.Section, blockCreate.Section);

            var responseBlock = await client.SendAsync(new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks/{blockCreated.BlockId}"),
            });
            Assert.Equal(HttpStatusCode.OK, responseBlock.StatusCode);

            var blockGet = await responseBlock.ReadAsAsync<BlockResponse>();
            Assert.NotNull(blockGet);

            Assert.Equal(blockGet.BlockId, blockCreated.BlockId);
            Assert.Equal(blockGet.Text, blockCreated.Text);
            Assert.Equal(blockGet.Title, blockCreated.Title);
            Assert.Equal(blockGet.Section, blockCreated.Section);

            var blockUpDate = new BlockRequest
            {
                Text = $"{blockGet.Text}-update-1",
                Title = $"{blockGet.Title}-update-1",
                Section = $"{blockGet.Section}-update-1",
            };
            var responseUpDate = await client.SendAsync(new HttpRequestMessage
            {
                Method = HttpMethod.Patch,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks/{blockCreated.BlockId}"),
                Content = new StringContent(JsonSerializer.Serialize(blockUpDate), Encoding.UTF8, MediaTypeNames.Application.Json),
            });
            Assert.Equal(HttpStatusCode.OK, responseUpDate.StatusCode);

            var blockUpdated = await responseUpDate.ReadAsAsync<BlockResponse>();
            Assert.NotNull(blockUpdated);
            Assert.Equal(blockUpdated.BlockId, blockCreated.BlockId);
            Assert.Equal(blockUpdated.Text, blockUpDate.Text);
            Assert.Equal(blockUpdated.Title, blockUpDate.Title);
            Assert.Equal(blockUpdated.Section, blockUpDate.Section);

            var responseGetUpdated = await client.SendAsync(new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks/{blockCreated.BlockId}"),
            });
            Assert.Equal(HttpStatusCode.OK, responseGetUpdated.StatusCode);

            var blockGetUpdated = await responseGetUpdated.ReadAsAsync<BlockResponse>();
            Assert.NotNull(blockGetUpdated);

            Assert.Equal(blockGetUpdated.BlockId, blockUpdated.BlockId);
            Assert.Equal(blockGetUpdated.Text, blockUpdated.Text);
            Assert.Equal(blockGetUpdated.Title, blockUpdated.Title);
            Assert.Equal(blockGetUpdated.Section, blockUpdated.Section);

            var responseDelete = await client.SendAsync(new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks/{blockCreated.BlockId}"),
            });
            Assert.Equal(HttpStatusCode.OK, responseDelete.StatusCode);

            var responseGetDelete = await client.SendAsync(new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks/{blockCreated.BlockId}"),
            });
            Assert.Equal(HttpStatusCode.NotFound, responseGetDelete.StatusCode);
            var message = await responseGetDelete.Content.ReadAsStringAsync();
            Assert.True(message.IndexOf($"Block with id = '{blockCreated.BlockId}' not exist.") > -1);
        }

        [Fact(DisplayName = "Add Block")]
        public async Task Add_Block()
        {
            var blockCreate = new BlockCreateRequest
            {
                Text = "Text-2",
                Title = "Title-2",
                Section = "Section-2",
            };

            using var scope = factory.Services.CreateScope();
            var scopedServices = scope.ServiceProvider;
            var blocksService = scopedServices.GetRequiredService<Bastion.Blocks.Abstractions.IBlocksService>();
            var blockCreated = await blocksService.AddBlockAsync(blockCreate.BlockId, blockCreate.Text, blockCreate.Title, "testUser", blockCreate.Section);

            Assert.NotNull(blockCreated);

            Assert.NotNull(blockCreated.BlockId);
            Assert.Equal(blockCreated.Text, blockCreate.Text);
            Assert.Equal(blockCreated.Title, blockCreate.Title);
            Assert.Equal(blockCreated.Section, blockCreate.Section);
        }
    }
}