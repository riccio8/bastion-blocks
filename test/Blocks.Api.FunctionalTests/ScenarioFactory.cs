﻿using Bastion.Authentication.Basic;
using Bastion.Blocks.Api.Constants;
using Bastion.Blocks.EntityFrameworkCore.Contexts;
using Bastion.Blocks.Stores;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;

namespace Blocks.Api.FunctionalTests
{
    [ExcludeFromCodeCoverage]
    public class ScenarioFactory<TEntryPoint>
        : WebApplicationFactory<TEntryPoint> where TEntryPoint : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.AddAuthorization(options
                    => options.AddPolicy(BlocksPolicyConstants.FullAccess, policy
                    => policy.RequireClaim(ClaimTypes.NameIdentifier, "admin")));
                services.AddAuthentication(BasicAuthenticationDefaults.AuthenticationScheme)
                    .AddBasicAuthentication();

                var descriptor = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<BlocksDbContext>));
                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }

                var dbName = "InMemoryDbForTesting:" + Guid.NewGuid().ToString();
                Console.WriteLine(dbName);
                services.AddDbContextPool<BlocksDbContext>(options
                    => options.UseInMemoryDatabase(dbName));

                var serviceProvider = services.BuildServiceProvider();
                using var scope = serviceProvider.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var dbContext = scopedServices.GetRequiredService<BlocksDbContext>();

                dbContext.Database.EnsureCreated();

                var blocks = dbContext.Blocks.ToList();

                if (!dbContext.Blocks.Any())
                {
                    dbContext.Blocks.AddRange(new Block[]
                    {
                        new Block
                        {
                            BlockId = "Block-1",
                            Project = "default",
                            Text = "Block-1-Text",
                            Title = "Block-1-Title",
                            Section = "Block-1-Section",
                            AddOrUpdateAt = DateTime.UtcNow,
                        },
                        new Block
                        {
                            BlockId = "Block-2",
                            Project = "default",
                            Text = "Block-2-Text",
                            Title = "Block-2-Title",
                            Section = "Block-2-Section",
                            AddOrUpdateAt = DateTime.UtcNow,
                        },
                        new Block
                        {
                            BlockId = "Block-3",
                            Project = "default",
                            Text = "Block-3-Text",
                            Title = "Block-3-Title",
                            Section = "Block-3-Section",
                            AddOrUpdateAt = DateTime.UtcNow,
                        },
                    });
                    dbContext.SaveChanges();
                }

                var blocks1 = dbContext.Blocks.ToList();
            });
        }
    }
}