using Bastion.Authentication.Basic;
using Bastion.Blocks.Api.Models.Requests;

using BastionBlocksSample.Api;

using Blocks.Api.FunctionalTests.Extensions;

using System;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

using Xunit;

namespace Blocks.Api.FunctionalTests
{
    [ExcludeFromCodeCoverage]
    public class VerifyAccessScenario : IClassFixture<ScenarioFactory<Startup>>
    {
        private readonly ScenarioFactory<Startup> factory;

        public VerifyAccessScenario(ScenarioFactory<Startup> factory)
        {
            this.factory = factory;
        }

        private const string loginUser = "user:password";
        private const string loginAdmin = "admin:password";

        private static AuthenticationHeaderValue CreateAuthenticationHeader(string login)
        {
            return login is null
                ? default
                : new AuthenticationHeaderValue(
                BasicAuthenticationDefaults.AuthenticationScheme,
                Convert.ToBase64String(Encoding.ASCII.GetBytes(login)));
        }

        [Theory(DisplayName = "Verify access when add block")]
        [InlineData(loginAdmin, HttpStatusCode.Created)]
        [InlineData(loginUser, HttpStatusCode.Forbidden)]
        [InlineData(default, HttpStatusCode.Unauthorized)]
        public async Task Verify_Access_When_Add_Block(
            string login,
            HttpStatusCode statusCode)
        {
            using var client = factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = CreateAuthenticationHeader(login);

            using var content = new BlockCreateRequest
            {
                Text = "Text-1",
                Title = "Title-1",
                Section = "Section-1",
                BlockId = "Block-1-verify",
            }.ToStringContent();

            using var response = await client.SendAsync(new HttpRequestMessage
            {
                Content = content,
                Method = HttpMethod.Post,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks"),
            });

            Assert.Equal(statusCode, response.StatusCode);
        }

        [Theory(DisplayName = "Verify access when update block")]
        [InlineData(loginAdmin, HttpStatusCode.OK)]
        [InlineData(loginUser, HttpStatusCode.Forbidden)]
        [InlineData(default, HttpStatusCode.Unauthorized)]
        public async Task Verify_Access_When_Update_Block(
            string login,
            HttpStatusCode statusCode)
        {
            using var client = factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = CreateAuthenticationHeader(login);

            using var content = new BlockRequest
            {
                Text = "Text-1-Update",
                Title = "Title-1-Update",
                Section = "Section-1-Update"
            }.ToStringContent();

            using var response = await client.SendAsync(new HttpRequestMessage
            {
                Content = content,
                Method = HttpMethod.Patch,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks/Block-3"),
            });

            Assert.Equal(statusCode, response.StatusCode);
        }

        [Theory(DisplayName = "Verify access when delete block")]
        [InlineData(loginAdmin, HttpStatusCode.OK)]
        [InlineData(loginUser, HttpStatusCode.Forbidden)]
        [InlineData(default, HttpStatusCode.Unauthorized)]
        public async Task Verify_Access_When_Delete_Block(
            string login,
            HttpStatusCode statusCode)
        {
            using var client = factory.CreateClient();
            client.DefaultRequestHeaders.Authorization = CreateAuthenticationHeader(login);

            using var response = await client.SendAsync(new HttpRequestMessage
            {
                Method = HttpMethod.Delete,
                RequestUri = new Uri($"{client.BaseAddress}api/v1/blocks/Block-1"),
            });

            Assert.Equal(statusCode, response.StatusCode);
        }
    }
}