using Bastion.Blocks.Extensions;

using System.Diagnostics.CodeAnalysis;

using Xunit;

namespace Blocks.Api.FunctionalTests
{
    [ExcludeFromCodeCoverage]
    public class ExtensionsUnitTest
    {
        [Theory(DisplayName = "Property get source or value")]
        [InlineData(null, null, null)]
        [InlineData("source", "", null)]
        [InlineData("source", null, "source")]
        [InlineData("source", "value", "value")]
        public void Property_Get_Source_Or_Value(string source, string value, string expected)
        {
            var result = PropertyExtensions.GetSourceOrValue(source, value);

            Assert.Equal(result, expected);
        }
    }
}