using Bastion.Blocks.Api.Responses;

using BastionBlocksSample.Api;

using Blocks.Api.FunctionalTests.Extensions;

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

using Xunit;

namespace Blocks.Api.FunctionalTests
{
    [ExcludeFromCodeCoverage]
    public class AnonymousScenario : IClassFixture<ScenarioFactory<Startup>>
    {
        private readonly ScenarioFactory<Startup> factory;

        public AnonymousScenario(ScenarioFactory<Startup> factory)
        {
            this.factory = factory;
        }

        [Fact(DisplayName = "Get blocks")]
        public async Task Get_Blocks()
        {
            var response = await factory.CreateClient()
                .GetAsync("api/v1/blocks");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var result = await response.ReadAsAsync<IEnumerable<BlockResponse>>();

            Assert.NotNull(result);
            Assert.True(result.Count() > 0);
        }

        [Fact(DisplayName = "Get block")]
        public async Task Get_Block()
        {
            var response = await factory.CreateClient()
                .GetAsync("api/v1/blocks/Block-2");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var result = await response.ReadAsAsync<BlockResponse>();

            Assert.NotNull(result);
        }
    }
}