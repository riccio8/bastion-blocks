﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace Bastion.Authentication.Basic
{
    [ExcludeFromCodeCoverage]
    public static class BasicAuthenticationExtensions
    {
        public static AuthenticationBuilder AddBasicAuthentication(
            this AuthenticationBuilder builder)
        {
            return builder.AddBasicAuthentication(BasicAuthenticationDefaults.AuthenticationScheme, _ => { });
        }

        public static AuthenticationBuilder AddBasicAuthentication(
            this AuthenticationBuilder builder,
            string authenticationScheme,
            Action<BasicAuthenticationOptions> configureOptions)
        {
            return builder.AddBasicAuthentication(authenticationScheme, BasicAuthenticationDefaults.AuthenticationDisplayName, configureOptions);
        }

        public static AuthenticationBuilder AddBasicAuthentication(
            this AuthenticationBuilder builder,
            string authenticationScheme,
            string displayName,
            Action<BasicAuthenticationOptions> configureOptions)
        {
            return builder.AddScheme<BasicAuthenticationOptions, BasicAuthenticationHandler>(authenticationScheme, displayName, configureOptions);
        }
    }

    [ExcludeFromCodeCoverage]
    public static class BasicAuthenticationDefaults
    {
        public const string AuthenticationScheme = "Basic";

        public const string AuthenticationDisplayName = null;

        public const string AuthorizationHeaderName = "Authorization";
    }

    [ExcludeFromCodeCoverage]
    public class BasicAuthenticationOptions : AuthenticationSchemeOptions
    {
    }

    [ExcludeFromCodeCoverage]
    public class BasicAuthenticationHandler : AuthenticationHandler<BasicAuthenticationOptions>
    {
        public BasicAuthenticationHandler(
            IOptionsMonitor<BasicAuthenticationOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock)
            : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey(BasicAuthenticationDefaults.AuthorizationHeaderName))
            {
                return AuthenticateResult.NoResult();
            }
            string authorizationHeader = Request.Headers[BasicAuthenticationDefaults.AuthorizationHeaderName];
            if (!authorizationHeader.StartsWith(BasicAuthenticationDefaults.AuthenticationScheme, StringComparison.OrdinalIgnoreCase))
            {
                return AuthenticateResult.NoResult();
            }
            string authorizationEncoded = authorizationHeader.Substring(BasicAuthenticationDefaults.AuthenticationScheme.Length + 1).Trim();
            if (string.IsNullOrEmpty(authorizationEncoded))
            {
                return AuthenticateResult.NoResult();
            }

            var (user, _) = DecodeUserAndPassword(authorizationEncoded);
            return AuthenticateResult.Success(new AuthenticationTicket(new ClaimsPrincipal(new ClaimsIdentity(new List<Claim>() { new Claim(ClaimTypes.NameIdentifier, user) }, Scheme.Name)), Scheme.Name));
        }

        private static (string user, string password) DecodeUserAndPassword(string authorizationEncoded)
        {
            var authorizationDecoded = Encoding.UTF8.GetString(Convert.FromBase64String(authorizationEncoded));
            var separator = authorizationDecoded.IndexOf(':');
            if (separator == -1)
            {
                throw new InvalidOperationException("Invalid Authorization header.");
            }
            return (authorizationDecoded.Substring(0, separator), authorizationDecoded.Substring(separator + 1));
        }
    }
}