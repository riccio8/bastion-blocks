﻿namespace Bastion.Blocks.Extensions
{
    /// <summary>
    /// Property extensions.
    /// </summary>
    public static class PropertyExtensions
    {
        /// <summary>
        /// Get source or value.
        /// </summary>
        /// <param name="source">Source string.</param>
        /// <param name="value">New value.</param>
        public static string GetSourceOrValue(this string source, string value)
        {
            return value is null ? source : string.IsNullOrEmpty(value) ? null : value;
        }
    }
}