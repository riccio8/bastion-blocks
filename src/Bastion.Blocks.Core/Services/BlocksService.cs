﻿using Bastion.Blocks.Abstractions;
using Bastion.Blocks.Extensions;
using Bastion.Blocks.Stores;

using System;
using System.Threading.Tasks;

namespace Bastion.Blocks.Services
{
    /// <summary>
    /// Blocks service.
    /// </summary>
    public class BlocksService : IBlocksService
    {
        private readonly IBlocksRepository blocksRepository;

        /// <summary>
        /// Initializes a new instance of <see cref="BlocksService"/>.
        /// </summary>
        public BlocksService(IBlocksRepository blocksRepository)
        {
            this.blocksRepository = blocksRepository;
        }

        /// <summary>
        /// Add block.
        /// </summary>
        /// <param name="blockId">Block id.</param>
        /// <param name="text">Text.</param>
        /// <param name="title">Title</param>
        /// <param name="userId">User id.</param>
        /// <param name="section">Section</param>
        public async Task<Block> AddBlockAsync(string blockId, string text, string title, string userId, string section)
        {
            var block = new Block
            {
                Text = text,
                Title = title,
                Section = section,
                AddOrUpdateAt = DateTime.UtcNow,
                Project = blocksRepository.Options.Project,
                BlockId = blockId ?? Guid.NewGuid().ToString()
            };

            blocksRepository.AddBlockHistory(block, userId);

            blocksRepository.AddBlock(block);

            await blocksRepository.SaveChangesAsync();

            return block;
        }

        /// <summary>
        /// Delete block.
        /// </summary>
        /// <param name="blockId">Block id.</param>
        /// <param name="userId">User id.</param>
        public async Task<Block> DeleteBlockAsync(string blockId, string userId)
        {
            var block = await blocksRepository.GetBlockAsync(blockId)
                ?? throw new ArgumentNullException($"Block with id = '{blockId}' not exist.");

            blocksRepository.AddBlockHistory(block, userId);

            blocksRepository.DeleteBlock(block);

            await blocksRepository.SaveChangesAsync();

            return block;
        }

        /// <summary>
        /// Get block.
        /// </summary>
        /// <param name="blockId">Block id.</param>
        public Task<Block> GetBlockAsync(string blockId)
        {
            return blocksRepository.GetBlockAsync(blockId);
        }

        /// <summary>
        /// Update block.
        /// </summary>
        /// <param name="blockId">Block id.</param>
        /// <param name="text">Text.</param>
        /// <param name="title">Title</param>
        /// <param name="userId">User id.</param>
        /// <param name="section">Section</param>
        public async Task<Block> UpdateBlockAsync(string blockId, string text, string title, string section, string userId)
        {
            var block = await blocksRepository.GetBlockAsync(blockId)
                ?? throw new ArgumentNullException($"Block with id = '{blockId}' not exist.");

            blocksRepository.AddBlockHistory(block, userId);

            block.AddOrUpdateAt = DateTime.UtcNow;
            block.Text = block.Text.GetSourceOrValue(text);
            block.Title = block.Title.GetSourceOrValue(title);
            block.Section = block.Section.GetSourceOrValue(section);
            blocksRepository.UpdateBlock(block);

            await blocksRepository.SaveChangesAsync();

            return block;
        }
    }
}