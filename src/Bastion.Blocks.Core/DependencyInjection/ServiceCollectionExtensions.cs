﻿using Bastion.Blocks.Abstractions;
using Bastion.Blocks.Services;

using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Service collection extensions for blocks service.
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add blocks service.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddBlocksService(
            this IServiceCollection services)
        {
            return services.AddBlocksService<BlocksService>();
        }

        /// <summary>
        /// Add custom blocks service.
        /// </summary>
        /// <typeparam name="TBlocksService">The type of blocks service to be registered.</typeparam>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddBlocksService<TBlocksService>(
            this IServiceCollection services)
            where TBlocksService : BlocksService
        {
            services.TryAddScoped<IBlocksService, TBlocksService>();

            return services;
        }
    }
}