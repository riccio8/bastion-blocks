﻿using System;

namespace Bastion.Blocks.Stores
{
    /// <summary>
    /// Block.
    /// </summary>
    public partial class Block
    {
        /// <summary>
        /// Block id.
        /// </summary>
        public string BlockId { get; set; }

        /// <summary>
        /// Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Section.
        /// </summary>
        public string Section { get; set; }

        /// <summary>
        /// Project.
        /// </summary>
        public string Project { get; set; }

        /// <summary>
        /// Add or update at.
        /// </summary>
        public DateTime AddOrUpdateAt { get; set; }
    }
}