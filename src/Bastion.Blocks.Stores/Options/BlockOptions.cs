﻿namespace Bastion.Blocks.Options
{
    /// <summary>
    /// Block options.
    /// </summary>
    public class BlockOptions
    {
        /// <summary>
        /// Project.
        /// </summary>
        public string Project { get; set; }
    }
}