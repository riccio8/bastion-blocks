﻿using Bastion.Blocks.Options;
using Bastion.Blocks.Stores;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bastion.Blocks.Abstractions
{
    /// <summary>
    /// Interface blocks repository.
    /// </summary>
    public interface IBlocksRepository
    {
        /// <summary>
        /// Options.
        /// </summary>
        BlockOptions Options { get; }

        /// <summary>
        /// Save changes.
        /// </summary>
        Task<int> SaveChangesAsync();

        /// <summary>
        /// Get blocks.
        /// </summary>
        Task<List<Block>> GetBlocksAsync();

        /// <summary>
        /// Get block.
        /// </summary>
        /// <param name="blockId">block id.</param>
        Task<Block> GetBlockAsync(string blockId);

        /// <summary>
        /// Add block.
        /// </summary>
        /// <param name="block">Block.</param>
        Block AddBlock(Block block);

        /// <summary>
        /// Update block.
        /// </summary>
        /// <param name="block">Block.</param>
        Block UpdateBlock(Block block);

        /// <summary>
        /// Delete block.
        /// </summary>
        /// <param name="block">Block.</param>
        Block DeleteBlock(Block block);

        /// <summary>
        /// Add history for block.
        /// </summary>
        /// <param name="block">Block.</param>
        /// <param name="userId">User id.</param>
        BlockHistory AddBlockHistory(Block block, string userId);
    }
}