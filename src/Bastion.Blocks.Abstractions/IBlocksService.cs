﻿using Bastion.Blocks.Stores;

using System.Threading.Tasks;

namespace Bastion.Blocks.Abstractions
{
    /// <summary>
    /// Interface blocks service.
    /// </summary>
    public interface IBlocksService
    {
        /// <summary>
        /// Add block.
        /// </summary>
        /// <param name="blockId">Block id.</param>
        /// <param name="text">Text.</param>
        /// <param name="title">Title</param>
        /// <param name="userId">User id.</param>
        /// <param name="section">Section</param>
        Task<Block> AddBlockAsync(string blockId, string text, string title, string userId, string section);

        /// <summary>
        /// Delete block.
        /// </summary>
        /// <param name="blockId">Block id.</param>
        /// <param name="userId">User id.</param>
        Task<Block> DeleteBlockAsync(string blockId, string userId);

        /// <summary>
        /// Get block.
        /// </summary>
        /// <param name="blockId">Block id.</param>
        Task<Block> GetBlockAsync(string blockId);

        /// <summary>
        /// Update block.
        /// </summary>
        /// <param name="blockId">Block id.</param>
        /// <param name="text">Text.</param>
        /// <param name="title">Title</param>
        /// <param name="userId">User id.</param>
        /// <param name="section">Section</param>
        Task<Block> UpdateBlockAsync(string blockId, string text, string title, string section, string userId);
    }
}