﻿namespace Bastion.Blocks.Api.Constants
{
    /// <summary>
    /// Blocks policy constants.
    /// </summary>
    public static class BlocksPolicyConstants
    {
        /// <summary>
        /// Full access.
        /// </summary>
        public const string FullAccess = "BlocksFullAccess";
    }
}