﻿using Bastion.Blocks.Abstractions;
using Bastion.Blocks.Api.Constants;
using Bastion.Blocks.Api.Extensions;
using Bastion.Blocks.Api.Models.Requests;
using Bastion.Blocks.Api.Responses;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Bastion.Blocks.Api.Controllers.Administrator
{
    /// <summary>
    /// Blocks controller.
    /// </summary>
    [Route("api/v1/blocks")]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class BlocksController : ControllerBase
    {
        private readonly IBlocksService blocksService;

        /// <summary>
        /// Initializes a new instance of <see cref="BlocksController"/>.
        /// </summary>
        public BlocksController(IBlocksService blocksService)
        {
            this.blocksService = blocksService;
        }

        /// <summary>
        /// Get block by id.
        /// </summary>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [ProducesResponseType(typeof(BlockResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetBlockAsync(string id)
        {
            var block = await blocksService.GetBlockAsync(blockId: id);
            if (block is null)
            {
                return NotFound($"Block with id = '{id}' not exist.");
            }

            return Ok(block.ToResponse());
        }

        /// <summary>
        /// Delete block.
        /// </summary>
        [HttpDelete("{id}")]
        [Authorize(Policy = BlocksPolicyConstants.FullAccess)]
        [ProducesResponseType(typeof(BlockResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> DeleteBlockAsync(string id)
        {
            var block = await blocksService.DeleteBlockAsync(blockId: id, userId: GetUserId());
            if (block is null)
            {
                return NotFound($"Block with id = '{id}' not exist.");
            }

            return Ok(block.ToResponse());
        }

        /// <summary>
        /// Update block.
        /// </summary>
        [HttpPatch("{id}")]
        [Authorize(Policy = BlocksPolicyConstants.FullAccess)]
        [ProducesResponseType(typeof(BlockResponse), StatusCodes.Status200OK)]
        public async Task<IActionResult> UpdateBlockAsync(string id, [FromBody] BlockRequest request)
        {
            var block = await blocksService.UpdateBlockAsync(
                blockId: id,
                text: request.Text,
                title: request.Title,
                userId: GetUserId(),
                section: request.Section);
            if (block is null)
            {
                return NotFound($"Block with id = '{id}' not exist.");
            }

            return Ok(block.ToResponse());
        }

        /// <summary>
        /// Add block.
        /// </summary>
        [HttpPost]
        [Authorize(Policy = BlocksPolicyConstants.FullAccess)]
        [ProducesResponseType(typeof(BlockResponse), StatusCodes.Status201Created)]
        public async Task<IActionResult> AddBlockAsync([FromBody] BlockCreateRequest request)
        {
            var block = await blocksService.AddBlockAsync(
                blockId: request.BlockId ?? Guid.NewGuid().ToString(),
                text: request.Text,
                title: request.Title,
                userId: GetUserId(),
                section: request.Section);

            return Created(nameof(GetBlockAsync), block.ToResponse());
        }

        private string GetUserId()
        {
            return User.FindFirst(ClaimTypes.NameIdentifier).Value;
        }
    }
}