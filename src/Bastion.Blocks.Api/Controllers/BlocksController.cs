﻿using Bastion.Blocks.Abstractions;
using Bastion.Blocks.Api.Extensions;
using Bastion.Blocks.Api.Responses;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Bastion.Blocks.Api.Controllers
{
    /// <summary>
    /// Blocks controller.
    /// </summary>
    [Route("api/v1/blocks")]
    [AllowAnonymous]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class BlocksController : ControllerBase
    {
        private readonly IBlocksRepository blocksRepository;

        /// <summary>
        /// Initializes a new instance of <see cref="BlocksController"/>.
        /// </summary>
        public BlocksController(IBlocksRepository blocksRepository)
        {
            this.blocksRepository = blocksRepository;
        }

        /// <summary>
        /// Get blocks.
        /// </summary>
        [HttpGet]
        [ProducesResponseType(typeof(IReadOnlyCollection<BlockResponse>), StatusCodes.Status200OK)]
        public async Task<IReadOnlyCollection<BlockResponse>> GetBlocksAsync()
        {
            var blocks = await blocksRepository.GetBlocksAsync();

            return blocks.Select(block => block.ToResponse()).ToList().AsReadOnly();
        }
    }
}