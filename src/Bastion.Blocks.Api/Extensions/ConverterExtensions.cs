﻿using Bastion.Blocks.Api.Responses;
using Bastion.Blocks.Stores;

namespace Bastion.Blocks.Api.Extensions
{
    internal static class ConverterExtensions
    {
        public static BlockResponse ToResponse(this Block block)
        {
            return block;
        }
    }
}