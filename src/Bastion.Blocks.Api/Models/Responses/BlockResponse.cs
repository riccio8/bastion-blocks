﻿using Bastion.Blocks.Api.Models;
using Bastion.Blocks.Stores;

using System;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Bastion.Blocks.Api.Responses
{
    /// <summary>
    /// Block response.
    /// </summary>
    [DataContract]
    public class BlockResponse : BlockBase
    {
        /// <summary>
        /// Implicit block to response conversion.
        /// </summary>
        /// <param name="block">Block.</param>
        public static implicit operator BlockResponse(Block block)
        {
            return new BlockResponse
            {
                Text = block.Text,
                Title = block.Title,
                BlockId = block.BlockId,
                Section = block.Section,
                Ts = block.AddOrUpdateAt,
            };
        }

        /// <summary>
        /// Block id.
        /// </summary>
        [JsonPropertyName("id")]
        [DataMember(Name = "id")]
        public string BlockId { get; set; }

        /// <summary>
        /// Updated.
        /// </summary>
        public DateTime Ts { get; private set; }
    }
}