﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Bastion.Blocks.Api.Models.Requests
{
    /// <summary>
    /// Block create request.
    /// </summary>
    [DataContract]
    public class BlockCreateRequest : BlockBase
    {
        /// <summary>
        /// Block id.
        /// </summary>
        [JsonPropertyName("id")]
        [DataMember(Name = "id")]
        public string BlockId { get; set; }
    }
}