﻿using System.Runtime.Serialization;

namespace Bastion.Blocks.Api.Models.Requests
{
    /// <summary>
    /// Block request.
    /// </summary>
    [DataContract]
    public class BlockRequest : BlockBase
    {
    }
}