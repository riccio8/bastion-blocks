﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Bastion.Blocks.Api.Models
{
    /// <summary>
    /// Block base.
    /// </summary>
    [DataContract]
    public class BlockBase
    {
        /// <summary>
        /// Title.
        /// </summary>
        [JsonPropertyName("title")]
        [DataMember(Name = "title")]
        public string Title { get; set; }

        /// <summary>
        /// Text.
        /// </summary>
        [JsonPropertyName("text")]
        [DataMember(Name = "text")]
        public string Text { get; set; }

        /// <summary>
        /// Section.
        /// </summary>
        [JsonPropertyName("section")]
        [DataMember(Name = "section")]
        public string Section { get; set; }
    }
}