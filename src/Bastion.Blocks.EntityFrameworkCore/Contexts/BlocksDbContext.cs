﻿using Bastion.Blocks.EntityFrameworkCore.Configurations;
using Bastion.Blocks.Stores;

using Microsoft.EntityFrameworkCore;

namespace Bastion.Blocks.EntityFrameworkCore.Contexts
{
    /// <summary>
    /// Notification database context.
    /// </summary>
    public partial class BlocksDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BlocksDbContext"/> class.
        /// </summary>
        public BlocksDbContext(DbContextOptions<BlocksDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Blocks.
        /// </summary>
        public virtual DbSet<Block> Blocks { get; set; }

        /// <summary>
        /// Block history.
        /// </summary>
        public virtual DbSet<BlockHistory> History { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new BlockConfiguration());
            modelBuilder.ApplyConfiguration(new BlockHistoryConfiguration());
        }
    }
}