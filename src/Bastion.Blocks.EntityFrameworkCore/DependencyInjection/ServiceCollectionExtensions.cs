﻿using Bastion.Blocks.Abstractions;
using Bastion.Blocks.EntityFrameworkCore.Contexts;
using Bastion.Blocks.EntityFrameworkCore.Repositories;
using Bastion.Blocks.Options;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection.Extensions;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Service collection extensions for blocks repository and dbContext.
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// Add blocks repository and dbContext.
        /// </summary>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddBlocksRepository(
            this IServiceCollection services,
            Action<BlockOptions> setupAction,
            Action<DbContextOptionsBuilder> optionsAction = null)
        {
            return services.AddBlocksRepository<BlocksRepository, BlocksDbContext>(setupAction, optionsAction);
        }

        /// <summary>
        /// Add custom blocks repository and dbContext.
        /// </summary>
        /// <typeparam name="TBlocksRepository">The type of blocks repository to be registered.</typeparam>
        /// <typeparam name="TBlocksDbContext">The type of context service to be registered.</typeparam>
        /// <param name="optionsAction">An optional action to configure the <see cref="DbContextOptions"/> for the context.</param>
        /// <param name="services">The <see cref="IServiceCollection"/> to add services to.</param>
        /// <returns>The same service collection so that multiple calls can be chained.</returns>
        public static IServiceCollection AddBlocksRepository<TBlocksRepository, TBlocksDbContext>(
            this IServiceCollection services,
            Action<BlockOptions> setupAction,
            Action<DbContextOptionsBuilder> optionsAction = null)
            where TBlocksRepository : BlocksRepository
            where TBlocksDbContext : BlocksDbContext
        {
            services.Configure(setupAction);
            services.AddDbContextPool<TBlocksDbContext>(optionsAction);
            services.TryAddScoped<IBlocksRepository, TBlocksRepository>();

            return services;
        }
    }
}