﻿using Bastion.Blocks.Abstractions;
using Bastion.Blocks.EntityFrameworkCore.Contexts;
using Bastion.Blocks.Options;
using Bastion.Blocks.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bastion.Blocks.EntityFrameworkCore.Repositories
{
    /// <summary>
    /// Blocks repository.
    /// </summary>
    public class BlocksRepository : IBlocksRepository
    {
        private readonly BlocksDbContext dbContext;

        /// <summary>
        /// Options.
        /// </summary>
        public BlockOptions Options { get; }

        /// <summary>
        /// Initializes a new instance of <see cref="BlocksRepository"/>.
        /// </summary>
        public BlocksRepository(
            BlocksDbContext dbContext,
            IOptions<BlockOptions> options)
        {
            Options = options.Value;
            this.dbContext = dbContext;
        }

        /// <summary>
        /// Get blocks.
        /// </summary>
        public Task<List<Block>> GetBlocksAsync()
        {
            return dbContext.Blocks
                .Where(x => x.Project == Options.Project)
                .OrderBy(x => x.BlockId)
                .AsNoTracking()
                .ToListAsync();
        }

        /// <summary>
        /// Get block.
        /// </summary>
        /// <param name="blockId">block id.</param>
        public Task<Block> GetBlockAsync(string blockId)
        {
            return dbContext.Blocks
                .FirstOrDefaultAsync(x => x.Project == Options.Project && x.BlockId == blockId);
        }

        /// <summary>
        /// Add block.
        /// </summary>
        /// <param name="block">Block.</param>
        public Block AddBlock(Block block)
        {
            dbContext.Blocks.Add(block);

            return block;
        }

        /// <summary>
        /// Update block.
        /// </summary>
        /// <param name="block">Block.</param>
        public Block UpdateBlock(Block block)
        {
            dbContext.Blocks.Update(block);

            return block;
        }

        /// <summary>
        /// Delete block.
        /// </summary>
        /// <param name="block">Block.</param>
        public Block DeleteBlock(Block block)
        {
            dbContext.Blocks.Remove(block);

            return block;
        }

        /// <summary>
        /// Add history for block.
        /// </summary>
        /// <param name="block">Block.</param>
        /// <param name="userId">User id.</param>
        public BlockHistory AddBlockHistory(Block block, string userId)
        {
            var blockHistory = new BlockHistory
            {
                UserId = userId,
                Text = block.Text,
                Title = block.Title,
                BlockId = block.BlockId,
                Section = block.Section,
                Project = block.Project,
                AddOrUpdateAt = block.AddOrUpdateAt,
                BlockHistoryId = Guid.NewGuid().ToString(),
            };

            dbContext.History.Add(blockHistory);

            return blockHistory;
        }

        /// <summary>
        /// Save changes.
        /// </summary>
        public Task<int> SaveChangesAsync()
        {
            return dbContext.SaveChangesAsync();
        }
    }
}