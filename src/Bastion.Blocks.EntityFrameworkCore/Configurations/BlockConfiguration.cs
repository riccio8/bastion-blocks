﻿using Bastion.Blocks.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Blocks.EntityFrameworkCore.Configurations
{
    internal class BlockConfiguration : IEntityTypeConfiguration<Block>
    {
        public void Configure(EntityTypeBuilder<Block> builder)
        {
            builder.HasKey(e => new { e.BlockId, e.Project })
                .HasName("block_pkey");

            builder.ToTable("blocks", "block");

            builder.Property(e => e.BlockId)
                .HasColumnName("block_id")
                .HasMaxLength(36)
                .ValueGeneratedNever();

            builder.Property(e => e.Section)
                .HasColumnName("section")
                .HasMaxLength(40);

            builder.Property(e => e.Project)
                .HasColumnName("project")
                .HasMaxLength(36);

            builder.Property(e => e.Text)
                .HasColumnName("text");

            builder.Property(e => e.Title)
                .HasColumnName("title");

            builder.Property(e => e.AddOrUpdateAt)
                .HasColumnName("ts")
                .IsRequired()
                .HasColumnType("timestamp(6) without time zone");
        }
    }
}