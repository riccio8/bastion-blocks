﻿using Bastion.Blocks.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Blocks.EntityFrameworkCore.Configurations
{
    internal class BlockHistoryConfiguration : IEntityTypeConfiguration<BlockHistory>
    {
        public void Configure(EntityTypeBuilder<BlockHistory> builder)
        {
            builder.HasKey(e => e.BlockHistoryId)
                .HasName("block_history_pkey");

            builder.ToTable("block_history", "block");

            builder.Property(e => e.BlockHistoryId)
                .HasColumnName("block_history_id")
                .HasMaxLength(36);

            builder.Property(e => e.BlockId)
                .HasColumnName("block_id")
                .HasMaxLength(36);

            builder.Property(e => e.Section)
                .HasColumnName("section")
                .HasMaxLength(40);

            builder.Property(e => e.UserId)
                .HasColumnName("user_id")
                .HasMaxLength(36);

            builder.Property(e => e.Project)
                .HasColumnName("project")
                .HasMaxLength(36);

            builder.Property(e => e.AddOrUpdateAt)
                .HasColumnName("ts")
                .IsRequired()
                .HasColumnType("timestamp(6) without time zone");

            builder.Property(e => e.Text)
                .HasColumnName("text");

            builder.Property(e => e.Title)
                .HasColumnName("title");
        }
    }
}